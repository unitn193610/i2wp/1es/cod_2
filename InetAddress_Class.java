package cod_2;

import java.net.InetAddress;
import java.net.UnknownHostException;

/*
--------> La classe InetAddress incapsula le informazioni degli indirizzi IP.
Supporta sia nomi simbolici che indirizzi numerici.
A tal riguardo possiede due particolari metodi:

    • getLocalHost(): rende un oggetto InetAddress rappresentante l'IP del localhost;
    • getByName(): rende un oggetto InetAddress relativo all'IP dell'hosto specificato come argomento.
 */

public class InetAddress_Class {

    public static void main(String[] args) {

        try {
            // Salvo sulla variabile 'addrress' l'IP dell'Host di tipo InetAddress.
            InetAddress address = InetAddress.getLocalHost();

            // Stampo l'oggetto address nello stream di output tramite il metodo toString()
            // per visualizzare cosa contiene tale oggetto.
            System.out.println("toString(): " + address.toString());
            // Dall'output noteremo che contiene due informazioni che possiamo suddividere in:

            // Stampo il nome dell'host locale
            System.out.println("Host name: " + address.getHostName());

            // Stampo l'IP dell'host locale
            System.out.println("Host address: " + address.getHostAddress());

        } catch (UnknownHostException ex) {
            System.err.println(String.format("Error while acquiring local host IP: %S", ex.getMessage()));
            ex.printStackTrace(System.err);
        }
    }
}